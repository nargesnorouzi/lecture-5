package edu.ucsc.cmps121.file;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void SaveText(View view){

        try {

            // open myfilename.txt for writing
            OutputStreamWriter out=new OutputStreamWriter(openFileOutput("myfilename.txt",MODE_APPEND));
            // write the contents to the file

            EditText ET = (EditText)findViewById(R.id.editText1);
            String text = ET.getText().toString();
            out.write(text);
            out.write('\n');

            // close the file

            out.close();

            Toast.makeText(this,"Text Saved !",Toast.LENGTH_LONG).show();
        }

        catch (java.io.IOException e) {

            //do something if an IOException occurs.
            Toast.makeText(this,"Sorry Text could't be added",Toast.LENGTH_LONG).show();


        }

    }

    public void ViewText (View view){

        StringBuilder text = new StringBuilder();


        try {
            // open the file for reading we have to surround it with a try

            InputStream inStream = openFileInput("myfilename.txt");//open the text file for reading

            // if file the available for reading
            if (inStream != null) {

                // prepare the file for reading
                InputStreamReader inputReader = new InputStreamReader(inStream);
                BufferedReader buffReader = new BufferedReader(inputReader);

                String line = "";
                //We initialize a string "line"

                while (( line = buffReader.readLine()) != null) {
                    //buffered reader reads only one line at a time, hence we give a while loop to read all till the text is null

                    text.append(line);
                    text.append('\n');    //to display the text in text line


                }}}

        //now we have to surround it with a catch statement for exceptions
        catch (IOException e) {
            e.printStackTrace();
        }

        //now we assign the text readed to the textview
        TextView tv = (TextView)findViewById(R.id.textView1);
        tv.setText(text);
    }


}
